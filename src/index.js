import * as upNumber from "./number";
import * as upObject from "./object";
import * as upStorage from "./storage";
import * as upString from "./string";
import * as utils from "./utils";
import computedEchartsOption from "./echarts-v4";

export {
  upString,
  upNumber,
  upObject,
  upStorage,
  utils,
  computedEchartsOption,
};
